﻿using Models;
using Models.Events;

namespace Benchmarks.Mechanisms
{
    public class DynamicEventApplicationMechanism : IEventApplicationMechanism
    {
        public void ApplyDomainEvent(EntityBase entity, IDomainEvent @event) 
        {
            ((dynamic) entity).Apply((dynamic)@event);
        }
    }
}