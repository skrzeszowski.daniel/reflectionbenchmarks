﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Models;
using Models.Events;

namespace Benchmarks.Mechanisms
{
    public class ReflectionWithPreRegistrationEventApplicationMechanism : IEventApplicationMechanism
    {
        private readonly Dictionary<Type, Dictionary<Type, MethodInfo>> _entitiesApplyMethods = new();
        
        public void RegisterFromAssembly(Type marker)
        {
            var entitiesTypes = marker.Assembly
                .GetTypes()
                .Where(x => !x.IsAbstract)
                .Where(x => typeof(EntityBase).IsAssignableFrom(x))
                .ToArray();

            foreach (var entityType in entitiesTypes)
            {
                _entitiesApplyMethods[entityType] = new Dictionary<Type, MethodInfo>();
                
                var entityApplyMethods = entityType.GetMethods()
                    .Where(x => x.Name == "Apply")
                    .Where(x => x.GetParameters().Length == 1)
                    .Select(x => (entityType: x, eventType: x.GetParameters()[0].ParameterType))
                    .Where(x => typeof(IDomainEvent).IsAssignableFrom(x.eventType))
                    .ToArray();

                foreach (var (applyMethod, eventType) in entityApplyMethods)
                {
                    _entitiesApplyMethods[entityType][eventType] = applyMethod;
                }
            }
        }
        
        public void ApplyDomainEvent(EntityBase entity, IDomainEvent @event)
        {
            _entitiesApplyMethods[entity.GetType()][@event.GetType()].Invoke(entity, new object?[]{ @event });
        }
    }
}