﻿using Models;
using Models.Events;

namespace Benchmarks.Mechanisms
{
    public class SwitchEventApplicationMechanism : IEventApplicationMechanism
    {
        public void ApplyDomainEvent(EntityBase entity, IDomainEvent @event) 
        {
            switch (entity)
            {
                case User user:

                    switch (@event)
                    {
                        case UserCreated userCreated:

                            user.Apply(userCreated);

                            break;
                        
                        case UserNameChanged userNameChanged:

                            user.Apply(userNameChanged);

                            break;
                        
                        case UserDeleted userDeleted:

                            user.Apply(userDeleted);

                            break;
                    }
                    
                    break;
            }
        }
    }
}