﻿using System.Linq;
using Models;
using Models.Events;

namespace Benchmarks.Mechanisms
{
    public class RuntimeReflectionEventApplicationMechanism : IEventApplicationMechanism
    {
        public void ApplyDomainEvent(EntityBase entity, IDomainEvent @event) 
        {
            var applyMethod = entity
                .GetType()
                .GetMethods()
                .Where(x => x.Name == "Apply")
                .Single(x =>
                    x.GetParameters().Length == 1 && x.GetParameters()[0].ParameterType == @event.GetType());

            applyMethod.Invoke(entity, new object?[] { @event });
        }
    }
}