﻿using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Engines;
using BenchmarkDotNet.Jobs;
using Benchmarks.Mechanisms;
using Models;

namespace Benchmarks
{
    [SimpleJob(RunStrategy.ColdStart, RuntimeMoniker.NetCoreApp50, id: "ColdStart")]
    [SimpleJob(RunStrategy.Throughput, RuntimeMoniker.NetCoreApp50, id: "Throughput")]
    [RPlotExporter]
    [MemoryDiagnoser]
    public class EventApplicationMechanismsComparison
    {
        private static readonly SwitchEventApplicationMechanism SwitchMechanism = new SwitchEventApplicationMechanism();
        private static readonly DynamicEventApplicationMechanism DynamicMechanism = new DynamicEventApplicationMechanism();
        private static readonly RuntimeReflectionEventApplicationMechanism RuntimeReflectionMechanism = new RuntimeReflectionEventApplicationMechanism();
        private static readonly ReflectionWithPreRegistrationEventApplicationMechanism ReflectionWithPreRegistrationMechanism = new ReflectionWithPreRegistrationEventApplicationMechanism();

        private static void TestMethod(IEventApplicationMechanism eventApplicationMechanism)
        {
            var user = User.Create("John", eventApplicationMechanism);

            user.ChangeName("Scott", eventApplicationMechanism);

            user.Delete(eventApplicationMechanism);

            var eventsStream = user.GetStream();

            var recreatedUser = new User();

            recreatedUser.LoadStream(eventsStream, eventApplicationMechanism);
        }

        [GlobalSetup]
        public void Setup()
        {
            ReflectionWithPreRegistrationMechanism.RegisterFromAssembly(typeof(User));
        }
        
        [Benchmark]
        public void Switch()
        {
            TestMethod(SwitchMechanism);
        }
        
        [Benchmark]
        public void Dynamic()
        {
            TestMethod(DynamicMechanism);
        }
        
        [Benchmark]
        public void RuntimeReflection()
        {
            TestMethod(RuntimeReflectionMechanism);
        }
        
        [Benchmark]
        public void ReflectionWithPreRegistration()
        {
            TestMethod(ReflectionWithPreRegistrationMechanism);
        }
    }
}