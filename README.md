# How to run

```
cd .\Benchmarks\
dotnet build -c Release
dotnet run -c Release
```

# Benchmark

``` ini

BenchmarkDotNet=v0.12.1, OS=Windows 10.0.18363.1440 (1909/November2018Update/19H2)
Intel Core i7-7700HQ CPU 2.80GHz (Kaby Lake), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.101
  [Host]     : .NET Core 5.0.1 (CoreCLR 5.0.120.57516, CoreFX 5.0.120.57516), X64 RyuJIT
  ColdStart  : .NET Core 5.0.1 (CoreCLR 5.0.120.57516, CoreFX 5.0.120.57516), X64 RyuJIT
  Throughput : .NET Core 5.0.1 (CoreCLR 5.0.120.57516, CoreFX 5.0.120.57516), X64 RyuJIT

Runtime=.NET Core 5.0  

```
|                        Method |        Job | RunStrategy | UnrollFactor |         Mean |           Error |          StdDev |      Median |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|------------------------------ |----------- |------------ |------------- |-------------:|----------------:|----------------:|------------:|-------:|------:|------:|----------:|
|                        Switch |  ColdStart |   ColdStart |            1 |  38,947.0 ns |    92,813.92 ns |   273,663.95 ns | 10,750.0 ns |      - |     - |     - |     424 B |
|                       Dynamic |  ColdStart |   ColdStart |            1 | 691,845.0 ns | 2,288,192.82 ns | 6,746,788.15 ns | 15,200.0 ns |      - |     - |     - |     424 B |
|             RuntimeReflection |  ColdStart |   ColdStart |            1 |  47,532.0 ns |    83,114.77 ns |   245,065.76 ns | 19,550.0 ns |      - |     - |     - |    4984 B |
| ReflectionWithPreRegistration |  ColdStart |   ColdStart |            1 |  32,381.0 ns |    71,002.46 ns |   209,352.36 ns |  8,500.0 ns |      - |     - |     - |     808 B |
|                        Switch | Throughput |  Throughput |           16 |     395.4 ns |         4.32 ns |         3.37 ns |    394.5 ns | 0.1349 |     - |     - |     424 B |
|                       Dynamic | Throughput |  Throughput |           16 |     720.9 ns |        14.35 ns |        22.34 ns |    733.1 ns | 0.1345 |     - |     - |     424 B |
|             RuntimeReflection | Throughput |  Throughput |           16 |   5,387.6 ns |        28.34 ns |        26.51 ns |  5,388.5 ns | 1.5869 |     - |     - |    4984 B |
| ReflectionWithPreRegistration | Throughput |  Throughput |           16 |   1,447.6 ns |         9.36 ns |         8.75 ns |  1,448.3 ns | 0.2575 |     - |     - |     808 B |


![alt text](https://gitlab.com/skrzeszowski.daniel/reflectionbenchmarks/-/raw/master/Benchmarks/BenchmarkDotNet.Artifacts/results/Benchmarks.EventApplicationMechanismsComparison-barplot.png "BarPlot")
