﻿using System;
using System.Collections.Generic;
using Models.Events;

namespace Models
{
    public class User : EntityBase
    {
        public Guid Id { get; } = Guid.Empty;
        public DateTimeOffset CreatedOn { get; private set; } = DateTimeOffset.MinValue;
        public string UserName { get; private set; } = string.Empty;
        public bool IsDeleted { get; private set; } = false;

        public User() { }
        private User(Guid id, DateTimeOffset createdOn, string userName, bool isDeleted)
        {
            Id = id;
            CreatedOn = createdOn;
            UserName = userName;
            IsDeleted = isDeleted;
        }

        public void ChangeName(string newName, IEventApplicationMechanism eventApplicationMechanism)
        {
            if (IsDeleted)
            {
                throw new InvalidOperationException("User is deleted");
            }

            Append(new UserNameChanged(Id, newName), eventApplicationMechanism);
        }

        public void Delete(IEventApplicationMechanism eventApplicationMechanism)
        {
            if (IsDeleted)
            {
                throw new InvalidOperationException("User is deleted");
            }
            
            Append(new UserDeleted(Id), eventApplicationMechanism);
        }

        public void Apply(UserCreated @event)
        {
            CreatedOn = @event.CreatedOn;
        }
        
        public void Apply(UserNameChanged @event)
        {
            UserName = @event.NewName;
        }
        
        public void Apply(UserDeleted @event)
        {
            IsDeleted = true;
        }

        public static User Create(string name, IEventApplicationMechanism eventApplicationMechanism)
        {
            var user = new User(
                Guid.NewGuid(),
                DateTimeOffset.MinValue,
                name,
                false);

            user.Append(new UserCreated(user.Id, DateTimeOffset.UtcNow), eventApplicationMechanism);
            
            return user;
        }
    }
}