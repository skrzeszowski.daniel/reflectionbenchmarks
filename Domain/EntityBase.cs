﻿using System.Collections.Generic;
using Models.Events;

namespace Models
{
    public abstract class EntityBase
    {
        private readonly List<IDomainEvent> _events = new ();
        
        protected void Append(IDomainEvent @event, IEventApplicationMechanism eventApplicationMechanism)
        {
            Apply(@event, eventApplicationMechanism);
            
            _events.Add(@event);
        }

        private void Apply(IDomainEvent @event, IEventApplicationMechanism eventApplicationMechanism)
        {
            eventApplicationMechanism.ApplyDomainEvent(this, @event);
        }


        public IEnumerable<IDomainEvent> GetStream()
        {
            return _events;
        }
        
        public void LoadStream(IEnumerable<IDomainEvent> eventsStream, IEventApplicationMechanism eventApplicationMechanism)
        {
            foreach (var @event in eventsStream)
            {
                Apply(@event, eventApplicationMechanism);
            }
        }
    }
}