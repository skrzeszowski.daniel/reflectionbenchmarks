﻿using System;

namespace Models.Events
{
    public class UserCreated : IDomainEvent
    {
        public Guid UserId { get; }
        public DateTimeOffset CreatedOn { get; }

        public UserCreated(Guid userId, DateTimeOffset createdOn)
        {
            UserId = userId;
            CreatedOn = createdOn;
        }
    }
}