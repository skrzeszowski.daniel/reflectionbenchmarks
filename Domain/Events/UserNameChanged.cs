﻿using System;

namespace Models.Events
{
    public class UserNameChanged : IDomainEvent
    {
        public Guid UserId { get; }
        public string NewName { get; }

        public UserNameChanged(Guid userId, string newName)
        {
            UserId = userId;
            NewName = newName;
        }
    }
}