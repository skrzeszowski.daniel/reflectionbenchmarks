﻿using System;

namespace Models.Events
{
    public class UserDeleted : IDomainEvent
    {
        public Guid UserId { get; }

        public UserDeleted(Guid userId)
        {
            UserId = userId;
        }
    }
}