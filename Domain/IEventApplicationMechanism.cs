﻿using Models.Events;

namespace Models
{
    public interface IEventApplicationMechanism
    {
        void ApplyDomainEvent(EntityBase entity, IDomainEvent @event);
    }
}